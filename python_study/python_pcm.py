"""
     2020-04-10
    生产者消费者模式是通过一个容器来解决生产者和消费者的强耦合问题。
    机制：生产者和消费者彼此之间不直接通讯，而是通过阻塞队列来进行通讯，所以生产者生产完数据之后不用等待消费者处理，
         直接扔给阻塞队列，消费者不直接找生产者要数据，而是从阻塞队列里取，阻塞队列就相当于一个缓冲区，平衡了生产者和消费者的处理能力，
         解耦了生产者和消费者。

    核心：是‘阻塞队列’也称消息队列。   分布式消息队列[RabbitMQ，RocketMq，Kafka]
    本例：Python内置的queue
"""
#-×- coding：utf-8   -*-
import time
import queue
import threading

q=queue.Queue(10)

def product(i):
    #厨师做
    while True:
        q.put('厨师%s 做的包子！'%i)
        time.sleep(2)


def consumer(j):
    #顾客吃
    while True:
        print('顾客%s吃了一个%s'%(j,q.get()))
        time.sleep(1)

for i in range(3):
    t=threading.Thread(target=product,args=(i,))
    t.start()


for j in range(10):
    v=threading.Thread(target=consumer,args=(j,))
    v.start()
