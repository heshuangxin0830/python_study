# # -*- coding: utf-8 -*-
#
from abc import ABC ,abstractclassmethod
from collections import namedtuple


Customer = namedtuple('Customer','name fidelity')

class lineItem:

      """初始化明细行：产品、数量、单价"""
      def __init__(self,product,qty,price):
          self.product = product
          self.qty = qty
          self.price = price


      def total(self):
          return self.price*self.qty


class Order:

    """客户、购物车、 促销活动"""
    def __init__(self, customer, cart, promotion=None):
        self.customer = customer
        self.cart = list(cart)
        self.promotion = promotion

    def total(self):
        """hasattr：用法：判断是否包含对应的属性"""
        if not hasattr(self,'__total'):
           self.__total = sum(item.total() for item in self.cart)   #计算购物车里总金额
        return self.__total


    def due(self):
        if self.promotion is None:
           discount=0
        else:
           discount = self.promotion.discount(self)
        return self.total()-discount



    def __repr__(self):
        fmt = '<Order total:{:.2f} due :{:.2f}>'
        return fmt.format(self.total(),self.due())


#

def rib(n):
    # assert n >= 0
    if n<=1:
       return 1
    else:
        return f(n)*f(n-1)












#
#
#
#
#
#
#
#
#
#


class Promotion(ABC):   #策略  抽象基类

      """
      @abstractmethod：抽象方法，含abstractmethod方法的类不能实例化，继承了含abstractmethod方法的子类必须复写所有abstractmethod装饰的方法，未被装饰的可以不重写
      @ property：方法伪装属性，方法返回值及属性值，被装饰方法不能有参数，必须实例化后调用，类不能调用
      @ classmethod：类方法，可以通过实例对象和类对象调用，被该函数修饰的方法第一个参数代表类本身常用cls，被修饰函数内可调用类属性，不能调用实例属性
      @staticmethod：静态方法，可以通过实例对象和类对象调用，被装饰函数可无参数，被装饰函数内部通过类名.属性引用类属性或类方法，不能引用实例属性
     """

      @abstractclassmethod
      def discount(self,order):
          """返回折扣金额、"""



#inhert Promotion
class FidelityPromo(Promotion):   #策略1
      """积分为1000或以上的客户提供5%的折扣"""
      def  discount(self,order):
          return order.total()*0.05 if order.customer.fidelity>=1000 else 0



class BulkItemPromo(Promotion):#策略2
     """单个商品为20个或以上时提供10%的折扣"""

     def discount(self,order):
         discount = 0
         for item in order.cart:
             if item.quantity>=20:
                discount+=item.total()*0.1
         return discount


class LargeOrderPromo(Promotion):#策略3
    """订单中不同商品达到10个或以上时提供7%的折扣"""

    def discount(self, order):
        discount_items = {item.product for item in order.cart}
        if len(discount_items)>=10:
           return order.total()*0.07
        return 0


#
#
# @property：方法伪装属性，方法返回值及属性值，被装饰方法不能有参数，必须实例化后调用，类不能调用
#@ property
#将一个方法伪装成属性，被修饰的特性方法，内部可以实现处理逻辑，但对外提供统一的调用方式，实现一个实例属性的get，set，delete三种方法的内部逻辑，具体含义看示
# -*- coding:utf-8 -*-

# -*- coding:utf-8 -*-

class Data:

    def __init__(self):
       self.number = 123

    @property
    def operation(self):
       return self.number

    @operation.setter
    def operation(self, number):
       self.number = number

    @operation.deleter
    def operation(self):
       del self.number




class Student(object):
      def get_score(self):
          return self._score

      def set_score(self,value):
          if not isinstance(value,int):
              raise ValueError('pppppp')
          if value < 0 or value > 100:
              raise ValueError('score must between 0 ~ 100!')
          self._score = value


Python内置的@property装饰器就是负责把一个方法变成属性调用的


class Student(object):

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, value):
        if not isinstance(value, int):
            raise ValueError('pppppp')
        if value < 0 or value > 100:
            raise ValueError('score must between 0 ~ 100!')
        self._score = value
s=Student()
s.score=60


#
# # ********************************************************************
# 类装饰器使用地方较少，核心是通过复写类的回调方法call.
#
# 装饰器应用场景
#
# 引入日志
# 函数执行时间统计
# 执行函数前预备处理
# 执行函数后清理功能
# 权限校验等场景
# 缓存
# # -*- coding:utf-8 -*-
#
class Add:

    def __init__(self, fn):
        print("初始化")
        self.num = 44
        self.fn = fn

    def __call__(self, *args, kwargs):
       print("类装饰器开始工作")
       return self.fn(self.num)

    @Add
    def test(n):
        return 4+n

if __name__ == '__main__':
    num = test()
    print(num)
#
#
# #----------------
#
# 初始化
# 类装饰器开始工作
# 48
#
# 类装饰器
# # @classmethod / @staticmethod 都是类装饰器
# # 源码：
#
# # classmethod 源码
# class classmethod(object):
#     """
#     classmethod(function) -> method
#
#     Convert a function to be a class method.
#
#     A class method receives the class as implicit first argument,
#     just like an instance method receives the instance.
#     To declare a class method, use this idiom:
#
#       class C:
#           @classmethod
#           def f(cls, arg1, arg2, ...):
#               ...
#
#     It can be called either on the class (e.g. C.f()) or on an instance
#     (e.g. C().f()).  The instance is ignored except for its class.
#     If a class method is called for a derived class, the derived class
#     object is passed as the implied first argument.
#
#     Class methods are different than C++ or Java static methods.
#     If you want those, see the staticmethod builtin.
#     """
#
#     def __get__(self, *args, **kwargs):  # real signature unknown
#         """ Return an attribute of instance, which is of type owner. """
#         pass
#
#     def __init__(self, function):  # real signature unknown; restored from __doc__
#         pass
#
#     @staticmethod  # known case of __new__
#     def __new__(*args, **kwargs):  # real signature unknown
#         """ Create and return a new object.  See help(type) for accurate signature. """
#         pass
#
#     __func__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
#
#     __isabstractmethod__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
#
#     __dict__ = None  # (!) real value is ''
#
# # ********************************************************************
#
#
#
# # 最简单装饰器:
# def func2(x):
#     print("func2 被调用")
#     print("x type = %s" % type(x))
#
#     def func1():
#         print("func1 被调用")
#         x()
#     return func1
#
#
# @func2  # 程序暂停,执行 func3 = func2(func3)
# def func3():
#     print("func3 被调用")
#
#
# func3()  # 调用函数
#
#
#
#
# # 复杂一点的装饰器
# print("start")  # 开始Debug
#
#
# # 函数定义 不会执行
# def func2(x):
#     print("func2 被调用")
#     print("x type = %s" % type(x))
#
#     def func1():
#         print("func1 被调用")
#         x()
#
#     return func1
#
# def func4(x):
#     print("func4 被调用")
#     print("x type = %s" % type(x))
#
#     def func5():
#         print("func5 被调用")
#         x()
#
#     return func5
#
#
#
#
# @func4
# @func2
# def func3():
#     print("func3 被调用")
#
#
# func3()  # 调用函数
#
# # 运行结果
# # start
# # func2 被调用
# # x type = <class 'function'>
# # func4 被调用
# # x type = <class 'function'>
# # func5 被调用
# # func1 被调用
# # func3 被调用
# #
# # 装饰过程:
# #
# # func3 是一个函数的定义压栈
# # @func2 得到func3 函数的引用 压栈
# # func2 被调用 func1 压栈  (  func2 被调用  x type = <class 'function'>)
# # @func4 的到func1 函数的引用 压栈
# # func4 被调用 func5 压栈   (  func4 被调用  x type = <class 'function'>)
# #
# # 调用过程:
# #
# # 装饰后调用func3 ,func3 指向func5 ,func5被调用
# #
# # func5 出栈,  func5 被调用
# # x(),x 指向func1 func1 被调用
# #
# #
# # func1 被调用 func4 出栈 ( 装饰过程,执行过,出栈无效果, 只有func4出栈, func1 在栈顶次可以调用) ,func1 被调用
# #
# # func1 出栈,  func1 被调用
# # x(),x 指向func3 func3 被调用
#
#
#
#案例00001  start
# 远程开机
def wake_up(request, mac='DC-4A-3E-78-3E-0A'):
    MAC = mac
    BROADCAST = "192.168.0.255"
    if len(MAC) != 17:
        raise ValueError("MAC address should be set as form 'XX-XX-XX-XX-XX-XX'")
    mac_address = MAC.replace("-", '')
    data = ''.join(['FFFFFFFFFFFF', mac_address * 20])  # 构造原始数据格式
    send_data = b''

    # 把原始数据转换为16进制字节数组，
    for i in range(0, len(data), 2):
        send_data = b''.join([send_data, struct.pack('B', int(data[i: i + 2], 16))])
    print(send_data)

    # 通过socket广播出去，为避免失败，间隔广播三次
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock.sendto(send_data, (BROADCAST, 7))
        time.sleep(1)
        sock.sendto(send_data, (BROADCAST, 7))
        time.sleep(1)
        sock.sendto(send_data, (BROADCAST, 7))
        return HttpResponse()
        print("Done")
    except Exception as e:
        return HttpResponse()
        print(e)
#
# #案例00001 ending
#
#
#
# # 职位爬虫代码   start
#
# import random
# import time
# import requests
# from openpyxl import Workbook
# import pymysql.cursors
#
#
# def get_conn():
#    '''建立数据库连接'''
#    conn = pymysql.connect(host='localhost',
#                                user='root',
#                                password='root',
#                                db='python',
#                                charset='utf8mb4',
#                                cursorclass=pymysql.cursors.DictCursor)
#    return conn
#
#
# def insert(conn, info):
#    '''数据写入数据库'''
#    with conn.cursor() as cursor:
#        sql = "INSERT INTO `python` (`shortname`, `fullname`, `industryfield`, `companySize`, `salary`, `city`, `education`) VALUES (%s, %s, %s, %s, %s, %s, %s)"
#        cursor.execute(sql, info)
#    conn.commit()
#
#
# def get_json(url, page, lang_name):
#    '''返回当前页面的信息列表'''
#    headers = {
#        'Host': 'www.lagou.com',
#        'Connection': 'keep-alive',
#        'Content-Length': '23',
#        'Origin': 'https://www.lagou.com',
#        'X-Anit-Forge-Code': '0',
#        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0',
#        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
#        'Accept': 'application/json, text/javascript, */*; q=0.01',
#        'X-Requested-With': 'XMLHttpRequest',
#        'X-Anit-Forge-Token': 'None',
#        'Referer': 'https://www.lagou.com/jobs/list_python?city=%E5%85%A8%E5%9B%BD&cl=false&fromSearch=true&labelWords=&suginput=',
#        'Accept-Encoding': 'gzip, deflate, br',
#        'Accept-Language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7'
#    }
#    data = {'first': 'false', 'pn': page, 'kd': lang_name}
#    json = requests.post(url, data, headers=headers).json()
#    list_con = json['content']['positionResult']['result']
#    info_list = []
#    for i in list_con:
#        info = []
#        info.append(i.get('companyShortName', '无'))  # 公司名
#        info.append(i.get('companyFullName', '无'))
#        info.append(i.get('industryField', '无'))   # 行业领域
#        info.append(i.get('companySize', '无'))  # 公司规模
#        info.append(i.get('salary', '无'))   # 薪资
#        info.append(i.get('city', '无'))
#        info.append(i.get('education', '无'))   # 学历
#        info_list.append(info)
#    return info_list   # 返回列表
#
#
# def main():
#    lang_name = 'python'
#    wb = Workbook()  # 打开 excel 工作簿
#    conn = get_conn()  # 建立数据库连接  不存数据库 注释此行
#    for i in ['北京', '上海', '广州', '深圳', '杭州']:   # 五个城市
#        page = 1
#        ws1 = wb.active
#        ws1.title = lang_name
#        url = 'https://www.lagou.com/jobs/positionAjax.json?city={}&needAddtionalResult=false'.format(i)
#        while page < 31:   # 每个城市30页信息
#            info = get_json(url, page, lang_name)
#            page += 1
#            time.sleep(random.randint(10, 20))
#            for row in info:
#                insert(conn, tuple(row))  # 插入数据库，若不想存入 注释此行
#                ws1.append(row)
#    conn.close()  # 关闭数据库连接，不存数据库 注释此行
#    wb.save('{}职位信息.xlsx'.format(lang_name))
#
# if __name__ == '__main__':
#    main()
#
# # 职位爬虫代码   ending
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


class MacroCommand:
    def __init__(self, commands):
        self.commands = lsit(commands)

    def __cell__(self):
        for command in self.commands:
            command()





def make_avarage():
    count=0
    total=0
    def averager(new_value):
        count+=1
        total+=new_value
        return total/count
    return averager





class Registry(obj):
    def __init__(self):
        self._funcs=[]

    def register(self,func):
        self._funcs.append(func)

    def run_all(self):
        return [func() for func in self._funcs]

r1=Registry()
r1=Registry()


@r1.register
def a():
    return 3


@r2.register
def b():
    return 5