# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
from datetime import datetime,timedelta


class accountant_income_expenditure(models.Model):
    _name = 'accountant.income.expenditure'
    _inherit = ['mail.thread']
    _description = u'账户流水'



    image = fields.Binary(
        "Image", attachment=True,
        help="This field holds the image used as image for the product, limited to 1024x1024px.")
    image_medium = fields.Binary(
        "Medium-sized image", attachment=True)
    image_small = fields.Binary(
        "Small-sized image", attachment=True)
    name = fields.Char(string='名称')
    # employee_id = fields.Many2one('hr.employee',string=u'使用者')
    user_id = fields.Many2one('hr.employee', string='使用者',track_visibility='onchange')  # 配送员

    # employee_id = fields.Char('使用者')
    thing_date = fields.Datetime(u'事件日期',track_visibility='onchange')
    in_out_put = fields.Selection([('in',u'入账'),('out',u'支出')],u'类型',track_visibility='onchange')
    amount = fields.Float(u'金额')
    is_loan= fields.Selection([('yes',u'是'),('no',u'否')],u'是否借款',track_visibility='onchange')
    notes = fields.Text(u'备注')
    line_ids = fields.One2many('accountant.income.expenditure.line','order_id',string=u'明细')
    leave_property = fields.Selection([('low','低'),('hight','高')],u'等级',track_visibility='onchange')
    state = fields.Selection([('draft', 'Draft'), ('in_queue', 'In Queue'), ('sending', 'Sending'), ('done', 'Sent')],
                             string='Status', required=True, copy=False, default='draft',
                             group_expand='_group_expand_states')
    employee_ids = fields.Many2one('hr.employee',string=u'使用者')





    def _group_expand_states(self, states, domain, order):
        return [key for key, val in type(self).state.selection]





class accountant_income_expenditure_line(models.Model):
    _name = 'accountant.income.expenditure.line'
    _description = u'账户出入明细'

    order_id = fields.Many2one('accountant.income.expenditure',u'父级流水')
    name = fields.Char(string='账户名称',help=u'车贷、房贷、公司贷。。。。。。')
    start_back_date = fields.Datetime(u'还款开始日期')
    end_back_date = fields.Datetime(u'还款结束日期')
    principal = fields.Float(u'本金')
    interest = fields.Float(u'利息')
    amount = fields.Float(u'月供')
    loan_period = fields.Integer(u'贷款期数')
    notes = fields.Text(u'备注')
