# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
from datetime import datetime,timedelta
import logging
_logger = logging.getLogger(__name__)

class personal_goal_management(models.Model):
    _name = 'personal.goal.management'
    _description = u'目标管理'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    

    image = fields.Binary(
        "Image", attachment=True,
        help="This field holds the image used as image for the product, limited to 1024x1024px.")
    image_medium = fields.Binary(
        "Medium-sized image", attachment=True)
    image_small = fields.Binary(
        "Small-sized image", attachment=True)
    name = fields.Char(string='名称')
    user_id = fields.Many2one('hr.employee', string='使用者',track_visibility='onchange')  # 配送员
    check_id = fields.Many2one('hr.employee', string='监督人',track_visibility='onchange')  # 配送员
    start_date = fields.Datetime(u'开始日期',track_visibility='onchange')
    end_date = fields.Datetime(u'结束日期',track_visibility='onchange')
    thing_type_id = fields.Many2one('skill.thing.type',string=u'类型',domain=[('skill_type','=','target')],track_visibility='onchange')
    notes = fields.Text(u'备注')
    state = fields.Selection([('draft',u'草稿'),('doing',u'进行中'),('done',u'完成')],u'状态',default='draft',track_visibility='onchange')
    line_ids = fields.One2many('personal.goal.implement.line','order_id',string=u'明细',track_visibility='onchange')
    leave_property = fields.Selection([('low','低'),('hight','高')],u'等级',track_visibility='onchange')
    # checkout_ids = fields.Many2many(
    #     'personal.skill.management',
    #     string='Checkouts')
    # message_subject = fields.Char()
    # message_body = fields.Html()

    # self.message_post('Hello!')
    # # 这会添加一个普通文本消息，但不会向follower发送通知。这是因为默认由mail.mt_note子类型发送消息。但我们可以通过指定的子类型来发送消息。要添加一条向follower发送通知的消息，应使用mt_comment子类型。另一个可选属性是消息标题，使用这两项的示例如下：
    # self.message_post('Hello again!', subject='Hello', subtype='mail.mt_comment')



    # @api.model
    # def default_get(self, field_names):
    #     defaults = super().default_get(field_names)
    #     checkout_ids = self.env.context.get('active_ids')
    #     defaults['checkout_ids'] = checkout_ids
    #     return defaults
    #
    # _logger.info(
    #     'Posted %d messages to Checkouts: %s',
    #     len(self.checkout_ids),
    #     str(self.checkout_ids),
    # )
    #
    #
    #
    # # 我们首先使用了super()
    # # 来调用标准的default_get()
    # # 运算，然后向默认值添加了一个checkout__id，而active_ids值从环境下文中读取。
    # #
    # # 下面我们需要实现点击表单中Send按钮的操作。
    # #
    # # 向导业务逻辑
    # # 除了无需进行任何操作仅仅关闭表单的
    # # Cancel
    # # 按钮外，我们还有一个Send按钮的操作需要实现。该按钮调用的方法为button_send，需要在wizard / checkout_mass_message.py文件中使用如下代码定义：
    #
    # @api.multi
    # def button_send(self):
    #     self.ensure_one()
    #     for checkout in self.checkout_ids:
    #         checkout.message_post(
    #             body=self.message_body,
    #             subject=self.message_subject,
    #             subtype='mail.mt_comment',
    #         )
    #     return True







class personal_goal_implement_line(models.Model):
    _name = 'personal.goal.implement.line'
    _description = u'具体计划明细'

    order_id = fields.Many2one('personal.goal.management',u'目标')
    name = fields.Char(string='实施目标')
    result = fields.Char(string='预期效果')
    start_date = fields.Datetime(string=u'开始日期')
    end_date = fields.Datetime(string=u'结束日期')
    notes = fields.Text(u'备注')




