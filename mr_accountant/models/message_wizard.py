# from . import checkout_mass_message
from odoo import api, exceptions, fields, models


class PersonMassMessage(models.TransientModel):
    _name = 'person.checkout.massmessage'
    _description = 'Send Message to Borrowers'



    checkout_ids = fields.Many2many(
        'personal.goal.management',
        string='Goal Management')
    message_subject = fields.Char()
    message_body = fields.Html()

    #
    #
    #
    # @api.model
    # def default_get(self, field_names):
    #     defaults = super().default_get(field_names)
    #     checkout_ids = self.env.context.get('active_ids')
    #     defaults['checkout_ids'] = checkout_ids
    #     return defaults
    #
    # _logger.info(
    #     'Posted %d messages to Checkouts: %s',
    #     len(self.checkout_ids),
    #     str(self.checkout_ids),
    # )
    #
    #
    #
    # # 我们首先使用了super()
    # # 来调用标准的default_get()
    # # 运算，然后向默认值添加了一个checkout__id，而active_ids值从环境下文中读取。
    # #
    # # 下面我们需要实现点击表单中Send按钮的操作。
    # #
    # # 向导业务逻辑
    # # 除了无需进行任何操作仅仅关闭表单的
    # # Cancel
    # # 按钮外，我们还有一个Send按钮的操作需要实现。该按钮调用的方法为button_send，需要在wizard / checkout_mass_message.py文件中使用如下代码定义：
    #
    @api.multi
    def button_send(self):
        self.ensure_one()
        for checkout in self.checkout_ids:
            checkout.message_post(
                body=self.message_body,
                subject=self.message_subject,
                subtype='mail.mt_comment',
            )
        return True


