# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.tools.translate import _
import xlrd, base64, datetime
import psycopg2


# class ZimoProjectShadowWizard(models.TransientModel):
#     _name = 'zimo.project.shadow.wizard'
#     _description = u'临时操作单'
#
#     project_id = fields.Many2one('project.project', string=u'项目')  # 项目
#     task_id = fields.Many2one('project.task', string=u'任务')  # 项目
#     stage_id = fields.Many2one('project.task.type', string='Stage')  # 阶段
#     super_staff_id = fields.Many2one('res.users', string=u'Superior staff')  # 上级人员
#     execute_id = fields.Many2one('res.users', string=u'执行人')
#     duty_id = fields.Many2one('res.users', string=u'责任人')
#     date_plan = fields.Date(string=u'计划日期')
#     date_done = fields.Date(string=u'完成日期')
#     cost_line_ids = fields.One2many('zimo.task.cost.line.wizard', 'wizard_id', 'cost line')  # 成本费用,收支项



#     @api.multi
#     def btm_confirm(self):
#         pass
#
#
# class ZimoTaskCostLineWizard(models.TransientModel):
#     _name = 'zimo.task.cost.line.wizard'
#     _description = u'成本'
#
#     wizard_id = fields.Many2one('zimo.project.shadow.wizard')
#     type_id = fields.Many2one('project.task.type')
#     name = fields.Char('cost name')
#     price = fields.Float('cost outlay')
#     paid = fields.Float('cost paid')  # 已支付
#     surplus_play = fields.Float(string=u'应付/应收')  # 应收或应付
#     partner_id = fields.Many2one('res.partner', string=u'业务伙伴')  # 业务伙伴 客户或供应商
#     taxes_id = fields.Many2one('account.tax', string=u'税率')  # 税率
#     play_type = fields.Selection([
#         ('expenditure', 'expenditure'),  # expenditure花费,支出
#         ('income', 'income'),  # income 收入
#     ], default='expenditure', string=u'收支')  # 收支类型
