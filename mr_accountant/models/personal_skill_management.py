# -*- coding: utf-8 -*-

from odoo import models, fields, api,_
from datetime import datetime,timedelta


class personal_skill_management(models.Model):
    _name = 'personal.skill.management'
    _inherit = ['mail.thread']
    _description = u'技能管理'




    @api.model
    def default_employee_id(self):
        return self.env['hr.employee'].search([('user_id' , '=',self._uid)],limit=1).id


    image = fields.Binary(
        "Image", attachment=True,
        help="This field holds the image used as image for the product, limited to 1024x1024px.")
    image_medium = fields.Binary(
        "Medium-sized image", attachment=True)
    image_small = fields.Binary(
        "Small-sized image", attachment=True)
    name = fields.Char(string='名称',track_visibility='onchange')
    user_id = fields.Many2one('hr.employee', string='记录者',default=default_employee_id,track_visibility='onchange')
    order_date = fields.Datetime(u'记录日期',default=lambda self: fields.datetime.now(),track_visibility='onchange')
    skill_type = fields.Many2one('skill.thing.type',string=u'类型',domain=[('skill_type','=','skill')],track_visibility='onchange')
    notes = fields.Text(u'备注')
    state = fields.Selection([('draft',u'草稿'),('doing',u'进行中'),('done',u'完成')],u'状态',default='draft',track_visibility='onchange')
    leave_property = fields.Selection([('low','低'),('mid','中'),('hight','高')],u'经验值',track_visibility='onchange')


