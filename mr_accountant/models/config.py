# -*- coding: utf-8 -*-

from odoo import models, fields, api,_,tools
from datetime import datetime,timedelta



MENU_ITEM_SEPARATOR = "/"



class skill_thing_type(models.Model):
    _name = 'skill.thing.type'
    _description = u'类型'


    _sql_constraints = [('name_uniq','unique(name)',u'名称不能重复')]


    name = fields.Char(string='名称')
    skill_type = fields.Selection([('target',u'目标'),('skill',u'技能')],u'状态',default='target')
    notes = fields.Text(u'备注')




class LevelTest(models.Model):
    _name = 'level.test'
    _description = u"层级显示模块"

    """层级显示模块"""

    name = fields.Char(u'地区')
    parent_id = fields.Many2one('level.test', u'父类')
    complete_name = fields.Char(compute='_compute_complete_name', string=u'全称', store=True)

    @api.depends('name', 'parent_id.complete_name')
    def _compute_complete_name(self):
        """层级关系1/2/3/4/5/6"""
        for menu in self:
            menu.complete_name = menu._get_full_name()

    def _get_full_name(self, level=6):
        if level <= 0:
            return '...'
        if self.parent_id:
            return self.parent_id._get_full_name(level - 1) + MENU_ITEM_SEPARATOR + (self.name or '')
        else:
            return self.name

    @api.multi
    def name_get(self):
        return [(menu.id,menu._get_full_name()) for menu in self]


    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise  ValidationError(u'错误,不能创建循环引用的类别')




class mr_mail_message(models.Model):
    _inherit = 'mail.message'
    _description = u'消息'





class mail_followers_summery(models.Model):
    _name = 'mail.followers.summery'
    _description = u'关注者单据'
    _auto = False



    model_id = fields.Many2one('ir.model',string=u'模型',index=True,compute='compute_mode_name')
    res_model = fields.Char('res model')
    mr_followers_count = fields.Integer(u'')
    partner_id = fields.Many2one('res.partner')
    res_ids = fields.Char('')


    def compute_mode_name(self):
        for order in self:
            order.model_id=self.env['ir.model'].search([('model','=',order.res_model)]).id



    #ROW_NUMBER()  over(ORDER BY partner_id) as id这个很重要
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW  mail_followers_summery as (
              SELECT
              ROW_NUMBER()  over(ORDER BY partner_id) as id,
                res_model,
                partner_id,
                count(*) as mr_followers_count,
                string_agg ( res_id || '', ',' ) AS res_ids 
            FROM
                mail_followers 
            GROUP BY
                res_model,
                partner_id
              )""")



    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=80, order=None):
        domain=domain or []
        domain.append(('partner_id','=',self.env.user.partner_id.id))
        fields=fields or []
        fields+=['res_ids','partner_id']
        result = super(mail_followers_summery, self).search_read(domain=domain, fields=fields, offset=0, limit=limit ,order=order)
        res=[]
        for r in result:
            ids=list(map(int, r['res_ids'].split(',')))
            count=len(self.env[r['res_model']].search([('id','in',ids),('create_uid','!=',self.env.user.id)]))
            if count>0:
               r['mr_followers_count']=count
               res.append(r)
        return  res




    @api.multi
    def btn_read_follow(self):
        self.ensure_one()
        res_ids =list(map(int, self.res_ids.split(',')))
        return {
                'type': 'ir.actions.act_window',
                'res_model': self.res_model,
                'view_mode': 'tree,form',
                'res_id': [('create_uid','!=',self.env.user.id),('id','in',res_ids)],
                'target': 'current',
                'name': self.model_id.name,
                }
