odoo.define('my_field_widget',function(require){
    "user strict"

    // 此方法：三个参数：1、A自定义：唯一的。2、是一个列表：可以不写【行模块之前需要加载的依赖项】。
    // 3、function(require)固定写法
    // odoo.define("A",[],function (require) {
    //         "user strict"
    //
    // });



var AbstractField = require('web.AbstractField');
var fieldRegistry = require('web.field_registry');


// 通过扩展AbstractField来扩展widget
var colorField = AbstractField.extend({
    // 设置css类，根元素标记和支持的字段类型
    className:'o_int_colorpicker',
    tagName:'span',
    supportedFieldTypes: ['integer'],

    // #获取js事件
    events:{
        'click.o_color_pill':'clickPill',
    },

    // 继承init，进行一些初始化
    init:function () {
        this.totalColors=10;
        this._super.apply(this,arguments);
    },


    // 继承_renderEdit、_renderReadonly以设置Dom元素
    // #编辑状态下操作
    _renderEdit:function () {
      this.$el.empty();
      for (var i=0; i<this.totalColors;i++){
          var className = "o_color_pill o_color_" + i;
          if (this.value===i){
              className += 'active'
          }
          this.$el.append($('<span>',{
             'class': className,
              'data-val': i,
          }));
      }
    },

    // 只读
    _renderReadonly:function () {
      var className = "o_color_pill active readonly o_color_"+ this.value;
      this.$el.append($('<span>',{
          'class':className,
      }));
    },
    // 处理程序
    clickPill:function(ev){
        var $target = $(ev.currentTarget);
        var data = $target.data();
        this._setValue(data.val.toString());
    }
});


// 注册你的widget：需要引入fieldRegistry模块
fieldRegistry.add('int_color',colorField);

// 使其可以用于其他附加
return {
    colorField:colorField,
};


});






// 例子2
// Creating a new field widget
odoo.define('my_field_widget', function (require) {
"use strict";

    // #获取某个模块用 require
    var FieldChar = require('web.basic_fields').FieldChar;

    //依赖的注册模块
    var fieldRegistry = require('web.field_registry');


    var CustomFieldChar = Fieldchar.extend({
        renderReadonly: function () {
        // implement some custom logic here
    },
    });

//在字段注册表中注册它
fieldRegistry.add('my-custom-field', CustomFieldChar);

 });





// 例子3：请注意，无需将小部件添加到注册表，因为它已经注册。
// Modifying an existing field widget
var basic_fields = require('web.basic_fields');
var Phone = basic_fields.FieldPhone;

Phone.include({
    events: _.extend({}, Phone.prototype.events, {
        'click': '_onClick',
    }),

    _onClick: function (e) {
        if (this.mode === 'readonly') {
            e.preventDefault();
            var phoneNumber = this.value;
            // call the number on voip...
        }
    },
});



// 从界面修改主窗口小部件)
var AppSwitcher = require('web_enterprise.AppSwitcher');
AppSwitcher.include({
    render: function () {
        this._super();
        // do something else here...
    },
});



// Adding a client action
// 客户端操作是一个小部件，它将控制菜单栏下方的屏幕部分。
var ControlPanelMixin = require('web.ControlPanelMixin');
var Widget = require('web.Widget');

var ClientAction = Widget.extend(ControlPanelMixin, {
    ...
});

// 如果您不需要，请不要添加控制面板mixin。 请注意，需要一些代码才能与控制面板交互（
// 通过mixin给出的update_control_panel方法）。
// Registering the client action:像往常一样，我们需要让Web客户端知道客户端操作和实际类之间的映射：

var core = require('web.core');
core.action_registry.add('my-custom-action', ClientAction);



// 第一步：将新视图类型添加到ir.ui.view的字段类型：
// Creating a new view (from scratch)
// 创建新视图是一个更高级的主题。
class View(models.Model):
    _inherit = 'ir.ui.view'
    type = fields.Selection(selection_add=[('map', "Map")])



// 第二步：将新视图类型添加到ir.actions.act_window.view的字段view_mode：
class ActWindowView(models.Model):
    _inherit = 'ir.actions.act_window.view'
    view_mode = fields.Selection(selection_add=[('map', "Map")])


// 第三步：先简单地扩展超类
// creating the four main pieces which makes a view (in JavaScript):
// 我们需要一个视图（AbstractView的子类，这是工厂），一个渲染器（来自AbstractRenderer）
// ，一个控制器（来自AbstractController）和一个模型（来自AbstractModel）。 先简单地扩展超类：
var AbstractController = require('web.AbstractController');
var AbstractModel = require('web.AbstractModel');
var AbstractRenderer = require('web.AbstractRenderer');
var viewRegistry = require('web.view_registry');
var AbstractView = require('web.AbstractView');

var MapController = AbstractController.extend({});
var MapRenderer = AbstractRenderer.extend({});
var MapModel = AbstractModel.extend({});
var MapView = AbstractView.extend({
    config: {
        Model: MapModel,
        Controller: MapController,
        Renderer: MapRenderer,
    },
});

// 注册  像往常一样，需要更新视图类型和实际类之间的映射：
viewRegistry.add('map', MapView);



// 第四步：implementing the four main classes:
// implementing the four main classes:
// View类需要解析arch字段并设置其他三个类。 Renderer负责在用户界面中表示数据，Model应该与服务器通信，加载数据并处理它。
// Controller在那里协调，与网络客户交谈，......
// creating some views in the database:

<record id="customer_map_view" model="ir.ui.view">
    <field name="name">customer.map.view</field>
    <field name="model">res.partner</field>
    <field name="arch" type="xml">
        <map latitude="partner_latitude" longitude="partner_longitude">
            <field name="name"/>
        </map>
    </field>
</record>





// Customizing an existing view
// 假设我们需要创建通用视图的自定义版本
// 例如，
// 一个看板视图，顶部有一些额外的带状(ribbon-like )小部件（显示一些特定的自定义信息）。
// 在这种情况下，这可以通过3个步骤完成：
// 1.扩展看板视图（也可能意味着扩展控制器/渲染器和/或模型），
// 2.然后在视图注册表中注册视图，
// 3.使用看板arch中的视图 （具体示例是帮助台仪表板）。




var HelpdeskDashboardRenderer = KanbanRenderer.extend({
    ...
});

var HelpdeskDashboardModel = KanbanModel.extend({
    ...
});

var HelpdeskDashboardController = KanbanController.extend({
    ...
});

var HelpdeskDashboardView = KanbanView.extend({
    config: _.extend({}, KanbanView.prototype.config, {
        Model: HelpdeskDashboardModel,
        Renderer: HelpdeskDashboardRenderer,
        Controller: HelpdeskDashboardController,
    }),
});

// adding it to the view registry:
// 像往常一样，我们需要通知Web客户端视图名称和实际类之间的映射。

var viewRegistry = require('web.view_registry');
viewRegistry.add('helpdesk_dashboard', HelpdeskDashboardView);



// using it in an actual view:
// 我们现在需要通知Web客户端特定的ir.ui.view需要使用我们的新类。 请注意，这是Web客户端特定的问题。 从服务器的角度来看，我们仍然有看板视图。 执行此操作的正确方法是在arch的根节点上使用特殊属性js_class（将在某天重命名为widget，因为这实际上不是一个好名称）：

<record id="helpdesk_team_view_kanban" model="ir.ui.view" >
    ...
    <field name="arch" type="xml">
        <kanban js_class="helpdesk_dashboard">
            ...
        </kanban>
    </field>
</record>










































































