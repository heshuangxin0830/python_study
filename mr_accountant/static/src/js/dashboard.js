odoo.define('target_dashboard.dashboard', function (require) {
"use strict";

var core = require('web.core');
var AbstractAction = require('web.AbstractAction');
var ControlPanelMixin = require('web.ControlPanelMixin');
var QWeb = core.qweb;

var _t = core._t;
var _lt = core._lt;

var EchartItem1=AbstractAction.extend({
    template: 'echartItem',
    events:{

    },
    init: function (parent, action, options) {
        this._super.apply(this, arguments);
        this.data1 = [];

    },

    start: function () {
        var self = this;
        var dom = self.$el.get(0);
        var myChart = echarts.init(dom);
        var option ={
            title : {
                text: '用户访问来源',
                subtext: '纯属虚构',
                x:'center'
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
            },
            series : [
                {
                    name: '访问来源',
                    type: 'pie',
                    radius : '55%',
                    center: ['50%', '60%'],
                    data:[
                        {value:335, name:'直接访问'},
                        {value:310, name:'邮件营销'},
                        {value:234, name:'联盟广告'},
                        {value:135, name:'视频广告'},
                        {value:1548, name:'搜索引擎'}
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        //使用刚刚指定配置项和数据显示图表
        myChart.setOption(option);
        return this._super.apply(this,arguments);
    },
});
//
var EchartItem2=AbstractAction.extend({
    template: 'echartItem',
    events:{

    },
    init: function (parent, action, options) {
        this._super.apply(this, arguments);
        this.data2 = [];

    },

    start: function () {
        var self = this;
        var dom = self.$el.get(0);
        var myChart3 = echarts.init(dom);
        var colors = ['#5793f3', '#d14a61', '#675bba'];
        var option = {
            color: colors,

            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross'
                }
            },
            grid: {
                right: '20%'
            },
            toolbox: {
                feature: {
                    dataView: {show: true, readOnly: false},
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            legend: {
                data:['蒸发量','降水量','平均温度']
            },
            xAxis: [
                {
                    type: 'category',
                    axisTick: {
                        alignWithLabel: true
                    },
                    data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: '蒸发量',
                    min: 0,
                    max: 250,
                    position: 'right',
                    axisLine: {
                        lineStyle: {
                            color: colors[0]
                        }
                    },
                    axisLabel: {
                        formatter: '{value} ml'
                    }
                },
                {
                    type: 'value',
                    name: '降水量',
                    min: 0,
                    max: 250,
                    position: 'right',
                    offset: 80,
                    axisLine: {
                        lineStyle: {
                            color: colors[1]
                        }
                    },
                    axisLabel: {
                        formatter: '{value} ml'
                    }
                },
                {
                    type: 'value',
                    name: '温度',
                    min: 0,
                    max: 25,
                    position: 'left',
                    axisLine: {
                        lineStyle: {
                            color: colors[2]
                        }
                    },
                    axisLabel: {
                        formatter: '{value} °C'
                    }
                }
            ],
            series: [
                {
                    name:'蒸发量',
                    type:'bar',
                    data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
                },
                {
                    name:'降水量',
                    type:'bar',
                    yAxisIndex: 1,
                    data:[2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
                },
                {
                    name:'平均温度',
                    type:'line',
                    yAxisIndex: 2,
                    data:[2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
                }
            ]
        };
        //使用刚刚指定配置项和数据显示图表
        myChart3.setOption(option);
        return this._super.apply(this,arguments);
    },
});



var TargetDashboard = AbstractAction.extend({
    template: 'target_darshboard',
    init: function (parent, action, options) {
        this._super.apply(this, arguments);
        this.datas1 = [];
        this.datas2 = [];
    },
    start:function () {
        var self = this;
        var chart1 = new EchartItem1(self,self.datas1,null);
        chart1.appendTo(self.$el);
        //
        var chart2 = new EchartItem2(self,self.datas2,null);
        chart2.appendTo(self.$el);

        return this._super.apply(this,arguments);

    }

});




core.action_registry.add('action_target_dashboard', TargetDashboard);
return TargetDashboard
});