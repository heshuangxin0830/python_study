from odoo import api, fields, models, _


class ShowMessageWizard(models.TransientModel):
    _name = "message.wizard"
    _description = "提示一下"

    def say_hello(self):
        context = dict(self._context or {})
        view_type = context.get('view_type')
        actived_id = context.get('actived_id')
        active_ids = context.get('active_ids')
        print("视图类型：", view_type)
        if view_type == "form":
            print("Form Selected ID:", actived_id)
        elif view_type == "list":
            print("Tree Selected IDs:", active_ids)
        else:
            print("其他视图类型的ID在JS里自行传值吧。")
        print("接下来做你想做的")