# # -*- coding: utf-8 -*-
# from xmlrpc import client
#
# # ***********************xml-rpc***********************
# class TestAPI():
#     """使用xml-rpc的第一个例子"""
#
#     def __init__(self, srv, port, db, user, pwd):
#         common = client.ServerProxy(
#             'http://%s:%d/xmlrpc/2/common' % (srv, port))
#         self.api = client.ServerProxy(
#             'http://%s:%d/xmlrpc/2/object' % (srv, port))
#         self.uid = common.authenticate(db, user, pwd, {})
#         self.pwd = pwd
#         self.db = db
#         self.model = 'personal.goal.management'
#
#
#     def execute(self, method, arg_list, kwarg_dict=None):
#         return self.api.execute_kw(
#             self.db, self.uid, self.pwd, self.model,
#             method, arg_list, kwarg_dict or {})
#
#
#     def search_read(self, text=None):
#         domain = [('name', 'ilike', text)] if text else []
#         fields = ['id', 'name']
#         return self.execute('search_read', [domain, fields])
#
#
#     def create(self, title):
#         vals = {'name': title}
#         return self.execute('create', [vals])
#
#
#     def write(self, title, id):
#         vals = {'name': title}
#         return self.execute('write', [[id], vals])
#
#
#     def unlink(self, id):
#         return self.execute('unlink', [[id]])
#
#
#
#
# if __name__ == '__main__':
#     # 测试配置
#     srv, db, port = 'localhost', 'odoo120325', 8069
#     user, pwd = 'admin', 'admin'
#     api = TestAPI(srv, port, db, user, pwd)
#     from pprint import pprint
#
#     pprint(api.search_read())
#
#
#
#
#
#
#
#
#
#
#
#
#
#
# # ****************************odoo-rpc****************************
# from odoorpc import ODOO
#
#
# class Test2API():
#     def __init__(self, srv, port, db, user, pwd):
#         self.api = ODOO(srv, port=port)
#         self.api.login(db, user, pwd)
#         self.uid = self.api.env.uid
#         self.model = 'personal.goal.management'
#         self.Model = self.api.env[self.model]
#
#     def execute(self, method, arg_list, kwarg_dict=None):
#         return self.api.execute(
#             self.model,
#             method, *arg_list, **kwarg_dict)
#
#         def search_read(self, text=None):
#             domain = [('name', 'ilike', text)] if text else []
#             fields = ['id', 'name']
#             return self.Model.search_read(domain, fields)
#
#         def create(self, title):
#             vals = {'name': title}
#             return self.Model.create(vals)
#
#         def write(self, title, id):
#             vals = {'name': title}
#             self.Model.write(id, vals)
#
#         def unlink(self, id):
#             return self.Model.unlink(id)
#
#
#
#
# if __name__ == '__main__':
#     # 测试配置
#     srv, db, port = 'localhost', 'odoo120325', 8069
#     user, pwd = 'admin', 'admin'
#     api = TestAPI(srv, port, db, user, pwd)
#     from pprint import pprint
#
#     pprint(api.search_read())
