# from argparse import ArgumentParser
# from test_api import TestAPI
#
#
# parser = ArgumentParser()
# parser.add_argument('command',choices=['list', 'add', 'set-title', 'del'])
# parser.add_argument('params', nargs='*')  # 可选参数
# args = parser.parse_args()
#
# # 这里
# # args
# # 是一个包含传入脚本参数的对象，args.command是提供的命令，args.params是可选项，用于存放命令所需的其它参数。如果使用了不存在或错误的命令，参数解析器会进行处理并提示用户应输入的内容。有关argparse更完整的说明，请参考官方文档。
# #
# # 下一步是执行所计划的操作。首先为
# # Odoo服务准备连接：
#
#
# srv, port, db = 'localhost', 8069, 'dev12'
# user, pwd = 'admin', 'admin'
# api = LibraryAPI(srv, port, db, user, pwd)
#
#
# if args.command == 'list':
#     text = args.params[0] if args.params else None
#     books = api.search_read(text)
#     for book in books:
#         print('%(id)d %(name)s' % book)
#
# # 这里我们使用了LibraryAPI.search_read()
# # 来从服务端获取图书记录列表。然后遍历列表中每个元素并打印。我们使用
# # Python
# # 字符串格式化来向用户显示每条图书记录，记录是一个键值对字典。下面添加add命令，这里使用了额外的书名作为参数：
# if args.command == 'add':
#     for title in args.params:
#         new_id = api.create(title)
#         print('Book added with ID %d.' % new_id)
#
#
# # 因为主要的工作已经在LibraryAPI对象中完成，下面我们只要调用write()
# # 方法并向终端用户显示结果即可。 set - title命令允许我们修改已有图书的书名，应传入两个参数，新的书名和图书的
# # ID：
# if args.command == 'set-title':
#     if len(args.params) != 2:
#         print("set command requires a title and ID.")
#     else:
#         book_id, title = int(args.params[0]), args.params[1]
#         api.write(title, book_id)
#         print('Title set for Book ID %d.' % book_id)
#
#
#
# # 最终我们要实现
# # del 命令来删除图书记录，学到这里应该不再有任何挑战性了：
# if args.command == 'del':
#     for param in args.params:
#         api.unlink(int(param))
#         print('Book with ID %s deleted.' % param)
