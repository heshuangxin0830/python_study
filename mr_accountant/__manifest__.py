# -*- coding: utf-8 -*-
{
    'name': "personal_management",

    'summary': """
               个人管理
             
             """,

    'description': """
            个人管理
    """,

    'author': "teddy",
    'website': "http://www.teddy.home",
    'category': 'study',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['web','website','hr','mail'],
    'qweb': [
        'static/src/xml/*.xml',
        # 'static/src/*.xml',

    ],
    # always loaded
    'data': [
        'data/data_bulid.xml',
        'security/ir.model.access.csv',
        'security/data_security.xml',
        'views/config_views.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/personal_goal_management_views.xml',
        'views/example_page.xml',
        'views/detail_page.xml',
        'views/personal_skill_management_views.xml',
        'views/message_wizard_views.xml',
        'wizard/widzard_employee.xml',
        'views/action.xml',
        'views/menus.xml',  # 菜单引用了action,需要先导入action,因此菜单放后面
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
    'application': True,
}
