ODOO项目模型框架：                                              个人管理APP
                                                                  |
                                                  —————————————————————————————————————
                                                 |                |                    |
                                              账单管理           目标管理              技能管理
                                                |||
                                              Dashboard



python基础知识：
     1、数据类型与结构
        Python3拥有
        1、Number：int、float、complex
           Python3中的整数只有int，没有long类型，int最大值如下，

           import sys
           max_size = sys.maxsize
           max_size = list(str(max_size))
           i=len(max_size)-4
           while i>0:
              max_size.insert(i,',')
              i-=4
           print ("".join(max_size))




        Boolean
     2、 String、
        下标:
            正数下标左向右，从0开始。
            负数下标右向左，从-1开始

        常用操作：
        # 左向右寻找指定的字符串，找不到返回-1
        print(mystr.find("world"))
        # 右向左寻找指定的字符串，找不到返回-1，
        # 返回的下标位置左向右
        print(mystr.rfind("world1"))
        # 左向右寻找指定的字符串，找不到报错
        print(mystr.index("world"))

        # 右向左寻找指定的字符串，找不到报错，
        # 返回的下标位置左向右
        print(mystr.rindex("world"))

        # 统计指定的字符串出现的次数，不存在为0
        print(mystr.count("world1"))

        replace
        # 将old_str 替换成new_str,不存在不报错
        print(mystr.replace("world1", "World"))

        startswith、endswith
        # 检查字符串是否是以 指定字符串 开头，不是返回False
        print(mystr.startswith("world1"))

        # 检查字符串是否是以 指定字符串 结尾，不是返回False
        print(mystr.endswith("world1"))

        lower、upper、capitalize、title
        # 将字符串中的所有字母小写
        print(mystr.lower())

        # 将字符串中的所有字母大写
        print(mystr.upper())

        # 将字符串的第一个字母大写，
        # 如果第一个字符不是字母，则方法无效
        print(mystr.capitalize())

        # 将字符串中的 单个字母 或 单词中的第一个字母 大写
        print(mystr.title())

        ljust、rjust、center
        # 使源字符串左对齐，并且不满足指定长度，则使用指定字符进行填充
        # 满足长度则源字符串不变
        print(mystr.ljust(50, "*"))

        # 使源字符串右对齐，并且不满足指定长度，则使用指定字符进行填充
        # 满足长度则源字符串不变
        print(mystr.rjust(50, "*"))

        # 使源字符串居中对齐，并且不满足指定长度，则使用指定字符进行填充
        # 满足长度则源字符串不变
        print(mystr.center(50, "*"))

        lstrip、rstrip、strip
        # 默认删除左端的空白字符\n\t\r等，
        # 也可以删除左端指定的字符或字符串
        print(mystr.lstrip("q"))

        # 默认删除右端的空白字符\n\t\r等，
        # 也可以删除右端指定的字符或字符串
        print(mystr.rstrip("world"))

        # 默认删除两端的空白字符\n\t\r等，
        # 也可以删除两端指定的字符或字符串
        print(mystr.strip("world1"))

        # 源字符串是否只包含空格，是返回True
        print(" ".isspace())

        partition、rpartition、splitlines、split
        # 使用指定字符串左向右搜索，将源字符串分割成三份，前中后，中即为指定字符串
        # 如果指定字符串不存在，则前为源字符串，中后两份为空字符串
        print(mystr.partition("world1"))

        # 使用指定字符串右向左搜索，将源字符串分割成三份，前中后，中即为指定字符串
        # 如果指定字符串不存在，则前中为空字符串，后为源字符串
        print(mystr.rpartition("world1"))

        # 根据换行符切割字符串，默认不显示换行符,当keepends=True 显示换行符
        print("hello\nhello\nhello\n".splitlines(keepends=True))

        # 使用指定字符切割字符串
        # 如果指定字符或字符串不存在，则不进行切割，但仍然返回列表。
        print(mystr.split(" "))

        isalpha、isalnum、isdecimal 、isdigit 、isnumeric
        # 源字符串所有字符都是字母 则返回 True,否则返回 False
        print(mystr.isalpha())

        # 源字符串所有字符都是字母或者数字 则返回 True,否则返回 False
        print(mystr.isalnum())

        # 源字符串所有字符都是不是数字
        # isdecimal < isdigit < isnumeric isnumeric可以判断的范围最大
        # isdecimal 普通数字  isdigit 特殊数字 isnumeric 中文数字
        print("1".isdecimal())
        print("1".isdigit())
        print("1".isnumeric())

        join
        # 将源字符串插入到列表中，组成新的字符串返回
        # 列表元素只有一项则返回第一项元素的值。
        print(mystr.join([" 1 ", " 1 "]))


        3、List：
        增加：list.append('a')  添加
        删除：list.pop()   删除最后一个
              del lists   删除变量
              lists.clear() 清空列表
        修改：list[0]='a'
        查找：print (info["sex")
        get,得到指定建的值
        info.get("age",20)
        Tuple：不可变列表
        Dictionary:
        案例01
        init_data_str = {"name": "Dragon", "sex": "F", "age": "20"}
        for i in init_data_str.keys():
            print("i = %s" % i)
        案例02
        init_data_str = {"name": "Dragon", "sex": "F", "age": "20"}
        for i in init_data_str.values():
            print("i = %s" % i)
        案例03
        init_data_str = {"name": "Dragon", "sex": "F", "age": "20"}
        for k, v in init_data_str.items():
            print("k = %-5s,v = %-5s" % (k, v))


        Set：集合(简单使用)
        集合是一个特殊的存在，它和列表、元组、字典都有点相似，但又不同。
        格式
        a=set()  # 初始化一个空集合并赋值给变量 a
        a={1，1，1}  # 使用大括号，存放单个元素，元素是一个一个值，不是键值对（区别于字典的键值对）
        特殊之处
        1。 不存在重复数据（列表、元组可以存储重复元素）
        2。 不能通过下标进行查看和修改（列表、元组可以查看，列表可以修改）
        3。 不能存储列表、字典、集合
        其它数据类型组成了Python3 的九种数据类型。



    2、函数
        案例001：
        def demo(arg1: int, arg2: str, arg3=None):
        """

        我是函数的文档说明

        :param arg1: int 参数1

        :param arg2: str 参数2

        :param arg3: 默认为None

        :return: None

        """

        # 鼠标左键点一下函数，Ctrl + Q
        pass

        案例0002
        函数的返回值
        什么是返回值？为什么需要返回值？
        在函数中，使用return 关键字返回的数据叫做函数的返回值。
        作用：为了得到函数的处理结果，比如希望得到两个数相加的结果

        带有返回值的函数
        def add2num(a, b):
            c = a+b
            return c

        案例0003
        函数间共享数据
        # 全局变量
        a = 0
        def func1():
            global a  # 在 func1 里修改全局变量
            a = 100
            print('func1----a=', a)

        def func2():
            print('func2----a=', a)  # 在 func2 里可以使用 func1 的计算结果

        def func3():
            global a  # 使用全局变量，很容易被第三者修改数据，导致错误
            a = 300
            print('func3----a=', a)
        func1()
        func3()
        func2()

        案例003
        函数的嵌套调用

        # 函数嵌套
        def func1():
            a = 100
            print('func1----a=', a)
            return a  # func1 将结果作为返回值处理


        def func2():
            a = func1()  # 在 func2 里直接调用 func1 并获取返回值
            print('func2----a=', a)


        def func3():
            global a
            a = 300
            print('func3----a=', a)


        func3()
        func2()

        案例004
        函数多返回值以及返回多值
        多返回值
        一个函数根据情况的不同，会出现多个return。
        def demo(age):
            if age > 17:
                return "成年了"
            elif age > 12:
                return "青少年"
            else:
                return "少年郎"

        返回多值
        同一个return，可以返回多个值。这里需要容器，来存放多个值。
        容器有 String（字符串）、List（列表）、Tuple（元组）、Dictionary（字典）、Set（集合），虽然有这么多容器 推荐使用List（列表）、Tuple（元组）、Dictionary（字典）。
        为什么不推荐使用String（字符串）、Set（集合）
         
        因为String（字符串）可以 split() 字符串，可以产生一个列表，所以不如直接使用列表
        Set（集合）不能存储重复数据，而且不能存储列表，字典，集合

        案例005
        函数参数
        缺省参数
        什么是缺省参数？ 调用函数时，缺省参数的值如果没有传入，则取默认值。
        def demo(args1,args2=""):  # args2 就是缺省参数
            pass

        不定长参数

        *args, 在变量args前使用一个 * (星号)，表示用元组接收无人接收的位置参数
        **kwargs，在变量kwargs前使用一个 ** (两个星号)，表示用字典接收无人接收的关键字参数

        def demo(args1, *args, **kwargs):
            print(args)

            print(kwargs)
        demo(1, 2, 3, 4, 5, name="qwer", age=17, sex="gg")

        1、可变类型 list、set、dict
            什么是可变类型

            可变，数据内容可变。因为数据内容可变，如果修改变量的值，则引用的地址不会发生变化

        2、不可变类型 number、bool、string、tuple、
            什么是不可变类型
            不可变，数据内容不可变。因为数据内容不可变，如果修改变量的值，则引用的地址会发生变化



       文件
        为什么需要文件？
        因为文件可以长期保存数据，省时省力。
        文件的操作
        文件的操作有 打开、关闭、读取、写入。
        路径
        访问文件需要知道文件的位置，也就是文件的路径。
        路径分为绝对路径和相对路径。
        绝对路径
        绝对路径是指从根目录开始的路径，win可以理解为从盘符开始的路径。
        相对路径
        一般是相对于当前目录（或文件夹）的路径，不是从根目录开始的路径，win不是从盘符开始的路径。
        文件的访问模式
        主要的 r（只读）、w（只写）、a（追加）


        r 模式
        默认模式，只读模式
        文件不存在，则程序报错
        文件存在，可以进行读取



        w 模式
        只写模式
        文件不存在，则创建一个新文件进行写入
        文件存在，每次打开都覆盖原文件，然后写入。



        a 模式
        追加模式，特殊的 w 模式
        文件不存在，则创建一个新文件进行写入
        文件存在，没有内容，则直接写入；有内容存在，在内容的末尾，进行追加写入。

        打开与关闭文件

        案例00001
        import os
        # 文件路径
        file_path = "DragonFang.txt"
        src_name = None

        # 是不是一个文件
        if os.path.isfile(file_path):
            src_name = open("DragonFang.txt")  # 是文件直接打开，默认r 模式
        else:
            src_name = open("DragonFang.txt", "w")  # 不是文件，创建文件
        src_name.close()  # 使用完关闭文件

        案例00002
        f = open('DragonFang.txt', 'w')  # 以只写模式打开
        f.write('Hello DragonFang!')  # write 将文件内容写入文件
        f.close()  # 关闭文件


        案例00003
        文件的相关操作
        import os
        # 文件的相关操作
        # 文件的重命名，原文件不存在报错
        os.rename("Dragon.txt", "DragonFang.txt")

        # 删除文件，文件不存在报错
        os.remove("Dragon.txt")

        # 判断指定资源是不是一个文件,是返回True，不存在不报错
        os.path.isfile("Dragon.txt")

        文件夹的相关操作
        import os

        # 创建文件夹，当文件夹不存在时创建，文件夹存在时报错
        os.mkdir("Dragon")

        # 删除文件夹，当文件夹存在时删除，文件夹不存在时报错
        os.rmdir("Dragon")

        # 获取当前工作目录
        os.getcwd()

        # 改变默认工作目录，"./" 表示当前目录  "../"表示上级目录
        os.chdir("../")

        # 获取目录列表
        os.listdir("./")

        # 判断指定路径是不是文件夹，是返回True，路径不存在不会报错
        os.path.isdir("")


        案例00005
        class People:
            def __init__(self, name, age):
                self.name = name
                self.age = age

            def show(self):
                print("name:%s ,age:%d " % (self.name, self.age))

            dragon_fang = People("DragonFang", 19)
            dragon_fang.show()

            xiao_ming = People("xiaoMing", 20)
            xiao_ming.show()

            # 结果为：
            # name:DragonFang ,age:19
            # name:xiaoMing ,age:20
        方法1：
        魔法方法_Str_(self) 的作用是打印对象的描述信息，必须有返回值，且返回值必须是字符串。

        方法2：
        魔法方法_del_(self) 当对象即将被销毁的时候执行。


        5、算术运算魔法方法
            当对象进行算术操作时，即触发对应的算术魔法方法
            1、__add__(self, other)， 定义加法行为 ： +
            2、__sub__(self, other) ，定义减法行为 ：-
            3、__mul__(self, other) ，定义乘法行为 ：*
            4、__truediv__(self, other) ，定义真除法行为 ：/

       6、属性访问控制
        __getattr__(self, name)：定义当用户试图访问一个不存在的属性时的行为
        class Test(object):
          def __init__(self,city):
              self.city = city
          def __getattr__(self,name):
              print('don\'t have the attribute:%s'%name)
        >>> a = Test('广州')
        >>> a.city
        '广州'
        >>> a.name
        don't have the attribute:name
        #这里并没有name属性，在找不到属性的情况下，正常的继承object的对象都会抛出AtrributeError的错误。但是这里通过__getattr__魔法方法改变了找不到属性时候的类的行为。输出了查找的属性的参数。


        __getattribute__(self, name)： 定义当该类的属性被访问时的行为（拦截所有的属性，包括存在的属性）。注意：避免"无限递归"的错误（返回时最好用调用基类的方法）

        class Test(object):
          def __init__(self,city):
              self.city = city
          def __getattribute__(self,name):
              print('__getattribute__：%s'%name)
              return name
        >>> a = Test('广州')
        >>> a.city #__getattribute__会拦截所有的属性,包括已存在的。
        __getattribute__：city
        '广州'
        >>> a.name
        __getattribute__：name
        'name'
















    3、异常与stock


    4、api开发


    5、web框架


    6、项目经验



    7、工作经验与技能











